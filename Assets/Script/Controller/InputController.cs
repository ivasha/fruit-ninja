﻿using Assets.Script.GameObjects;
using UnityEngine;

namespace Assets.Script.Controller
{
    public delegate void MouseEventHandler(Vector3 mousePosition);
    public delegate void ButtonEventHandler(bool up);

    public class InputController : MonoBehaviour
    {

        public static event MouseEventHandler TrackerMousePosition;
        public static event ButtonEventHandler TrackerButton;

        private Vector2 fingerPos;
        private Camera mainCamera;

        void Start()
        {
            mainCamera = Camera.main;
        }

        void Update()
        {
          CheckForMouseMovement();
        }

        private void CheckForMouseMovement()
        {
            fingerPos = Input.mousePosition;
            Vector3 mPosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, 5);
            OnMousePositionEvent(mPosition);

            if (Input.GetMouseButtonDown(0))
            {
                OnButtonEvent(true);
            }

            if (Input.GetMouseButtonUp(0))
            {
                OnButtonEvent(false);
            }
        }

        private void OnMousePositionEvent(Vector3 mousePosition)
        {
            if (TrackerMousePosition != null)
            {
                TrackerMousePosition(mousePosition);
            }
        }

        private void OnButtonEvent(bool up)
        {
            if(TrackerButton != null )
            {
                TrackerButton(up);
            }
        }
    }
}

