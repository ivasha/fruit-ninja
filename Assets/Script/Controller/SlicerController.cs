﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Script.GameObjects;


namespace Assets.Script.Controller
{
    public class SlicerController : MonoBehaviour
    {
        Slicer slicer;
        public Color StartColor = Color.yellow;
        public Color EndColor = Color.red;
        public float StartWidth = 0.1f, EndWidth = 0f;

        private AudioSource audioSlice;
        private int maxQueueSize;
        private float sliceTime;
        private Queue<Vector3> positionQueue = new Queue<Vector3>();


        // Use this for initialization
        void Start()
        {
            slicer = Slicer.Instance;
            slicer.TrailRenderer.material = new Material(Shader.Find("Particles/Additive"));
            slicer.TrailRenderer.startColor = StartColor;
            slicer.TrailRenderer.endColor = EndColor;
            slicer.TrailRenderer.startWidth = StartWidth;
            slicer.TrailRenderer.endWidth = EndWidth;
            slicer.TrailRenderer.time = 0.2f;
            slicer.TrailRenderer.minVertexDistance = 0.01f;

            maxQueueSize = 200;
            sliceTime = 0.02f;
            audioSlice = GetComponent<AudioSource>();

            StartCoroutine(AddSlicerPositionToQueue());

            InputController.TrackerMousePosition += InputController_TrackerMousePosition;
            InputController.TrackerButton += InputController_TrackerButton;
        }

        private void InputController_TrackerButton(bool up)
        {
            if(up)
            {
                slicer.ActivateSliceDevicce();
            } else
            {
                slicer.DisableSlicingDevice();
            }
        }

        private void InputController_TrackerMousePosition(Vector3 mousePosition)
        {
            slicer.Position = Camera.main.ScreenToWorldPoint(mousePosition);
        }

        // Update is called once per frame
        void Update()
        {
            Hit();
        }

        private void Hit()
        {
            if (positionQueue.Count == 0) return;
            if (!slicer.IsActive()) return;
            slicer.RigidBody.position = Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y));
            
            //Debug.DrawLine(slicer.Position, positionQueue.Peek(), Color.red);
            RaycastHit hit;

            if (Physics.Linecast(slicer.Position, positionQueue.Peek(), out hit))
            {
                audioSlice.Play();
                Fruit apple = hit.collider.gameObject.GetComponent<Fruit>();
                apple.DestroyFruit();
            }
        }
            

        private IEnumerator AddSlicerPositionToQueue()
        {
            while (true)
            {
                if (slicer.IsActive())
                {
                    if (positionQueue.Count >= maxQueueSize)
                    {
                        positionQueue.Dequeue();

                    }
                    Vector3 position = slicer.Position;
                    positionQueue.Enqueue(position);
                }
                else
                {
                    positionQueue.Clear();
                }
                yield return new WaitForSeconds(sliceTime);

            }
        }
    }
}
