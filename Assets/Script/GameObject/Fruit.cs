﻿using UnityEngine;

namespace Assets.Script.GameObjects
{
    public abstract class Fruit : MonoBehaviour
    {
        public int value = 0;

        [SerializeField]
        private GameObject _splash;

        protected Vector3 _randomPos;
        protected Vector3 position;

        public Rigidbody RigidBody;

        public delegate void DestroyFruitEventHandler (Fruit fruit);
        public static event DestroyFruitEventHandler DestroyFruitEvent;

        protected virtual void Start()
        {
            RigidBody = gameObject.GetComponent<Rigidbody>();
        }

        // Update is called once per frame
        protected void Update()
        {
            if (gameObject.transform.position.y < -10)
            {
                gameObject.SetActive(false);
                DisableRigidBody();
            }
        }

        public void DestroyFruit()
        {
            if (DestroyFruitEvent != null)
            {
                DestroyFruitEvent(this);
            }

           gameObject.SetActive(false);
        }

        private void DisableRigidBody()
        {
            RigidBody.isKinematic = true;
            RigidBody.detectCollisions = false;
        }
    }
}
