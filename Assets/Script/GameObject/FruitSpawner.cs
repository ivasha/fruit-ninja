﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FruitSpawner : MonoBehaviour {

    public float Time = 3;
    private int _force = 8;

    // Use this for initialization
    void Start ()
    {
        StartCoroutine(SpawnFruit());
    }

    private IEnumerator SpawnFruit()
    {
        while (true)
        {
            for (int i = 0; i < 4; i++)
            {
                GameObject fruit = ObjectPooler.Instance.GetPooledGameObject("Fruit");
                fruit.SetActive(true);

                fruit.transform.position = new Vector3(Random.Range(-1.5f, 2f), Random.Range(-4f, 0f), -7.3f);
                fruit.transform.rotation = Quaternion.identity;
                fruit.GetComponent<Rigidbody>().isKinematic = false;
                fruit.GetComponent<Rigidbody>().detectCollisions = true;

                fruit.GetComponent<Rigidbody>().AddForce(fruit.transform.up * _force, ForceMode.Impulse);
            }

            yield return new WaitForSeconds(Time);
        }
       
    }
}
