﻿using System.Collections.Generic;
using UnityEngine;

public class ObjectPooler : MonoBehaviour
{

    public static ObjectPooler Instance;
    public List<ObjectPoolItem> itemsToPool;


    public List<GameObject> _pooledObjects;

    private void Awake()
    {
        Instance = this;
    }

    private void Start()
    {
        _pooledObjects = new List<GameObject>();
        foreach (ObjectPoolItem item in itemsToPool)
        {
            for (int i = 0; i < item.ObjectPooledCount; i++)
            {
                GameObject obj = CreatePooledObject(item);
                _pooledObjects.Add(obj);
            }
        }
    }

    public GameObject GetPooledGameObject(string tag)
    {
        foreach (GameObject tempGameObject in _pooledObjects)
        {
            if (!tempGameObject.activeInHierarchy && tempGameObject.CompareTag(tag))
            {
                return tempGameObject;
            }
        }
        foreach (ObjectPoolItem item in itemsToPool)
        {
            if (item.Prefab.tag == tag)
            {
                if (item.ShouldExpand)
                {
                    GameObject obj = CreatePooledObject(item);
                    _pooledObjects.Add(obj);
                    return obj;
                }
            }
        }
        return null;
    }

    private GameObject CreatePooledObject(ObjectPoolItem item)
    {
        GameObject obj = (GameObject) Instantiate(item.Prefab);
        obj.SetActive(false);
        return obj;
    }

    [System.Serializable]
    public class ObjectPoolItem
    {
        public int ObjectPooledCount;
        public GameObject Prefab;
        public bool ShouldExpand;
    }

}


