﻿using UnityEngine;

public class Slicer 
{
    private static Slicer instance;
    public TrailRenderer TrailRenderer;

    private GameObject lineGO;
    private LayerMask sliceableObjects;
    private BoxCollider sliceCollider;
    public Rigidbody RigidBody;


    public Vector3 Position
    {
        get { return lineGO.transform.position; }
        set { lineGO.transform.position = value; }
    }

    private Slicer()
    {
        lineGO = new GameObject("Line");
        RigidBody = lineGO.AddComponent<Rigidbody>();
        RigidBody.useGravity = false;
        TrailRenderer = lineGO.AddComponent<TrailRenderer>();
        DisableSlicingDevice();
    }

    public static Slicer Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new Slicer();
            }
            return instance;
        }
    }

    public void ActivateSliceDevicce()
    {
        lineGO.SetActive(true);
    }

    public void DisableSlicingDevice()
    {
        if (lineGO.activeInHierarchy)
        {
            lineGO.SetActive(false);
        }
    }

    public bool IsActive()
    {
        if (!lineGO.activeInHierarchy) return false;
        return true;
    }
}
