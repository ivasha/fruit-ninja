﻿using Assets.Script.GameObjects;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Splash : MonoBehaviour {

    [SerializeField]
    private float offset = 0.2f;

    private Color randomAlpha;
    private float currentAlpha;
    private Color _color;

    private void Awake()
    {
        _color = GetComponent<Renderer>().material.color;
        Fruit.DestroyFruitEvent += Fruit_DestroyFruitEvent;
    }

    void Start()
    {
        randomAlpha = new Color(1, 1, 1, Random.Range(0.3f, 0.5f));
        _color = randomAlpha;
        InvokeRepeating("ReduceAlpha", 0.3f, 0.5f);
    }

    private void Fruit_DestroyFruitEvent(Fruit fruit)
    {
        Vector3 positionOffset = new Vector3(0, offset, 0);
        GameObject splash = ObjectPooler.Instance.GetPooledGameObject("Splash");
        splash.transform.position = fruit.transform.position + positionOffset;
        splash.transform.rotation = Quaternion.identity;
        splash.SetActive(true);
    }

    void ReduceAlpha()
    {
        currentAlpha = _color.a;

        if (_color.a <= 0.1f)
        {
            gameObject.SetActive(false);
        }
        else
        {
            _color = new Color(1, 1, 1, currentAlpha - 0.1f);
        }
    }

}
