﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[RequireComponent(typeof(UITimer))]
public class Timer : MonoBehaviour {

    public GameObject alertReference;
    private UITimer _timeDisplay;
    private int _time;

    private void Awake()
    {
        _timeDisplay = GetComponent<UITimer>();
    }

    void Start ()
    {
        InvokeRepeating("ReduceTime", 1, 1);
        if (_timeDisplay)
        {
            _time = int.Parse(_timeDisplay.GetTimeText());
        }
    }
	
    private void ReduceTime()
    {
        if (_time == 1)
        {
            Time.timeScale = 0;
            Instantiate(alertReference, new Vector3(0, 1f, -5f), transform.rotation);
        }
        _time--;
        _timeDisplay.SetTextTime(_time.ToString());

    }

}
