﻿using Assets.Script.GameObjects;
using UnityEngine;
using UnityEngine.UI;

public class Score : MonoBehaviour {

    [SerializeField]
    private Text scoreText;
    private float  score;

    private void Start()
    {
        scoreText = GetComponent<Text>();
        Fruit.DestroyFruitEvent += Fruit_DestroyFruitEvent;
    }

    private void Fruit_DestroyFruitEvent(Fruit fruit)
    {
        score += fruit.value;
        SetScoreText();
    }

    private void SetScoreText()
    {
        scoreText.text = score.ToString();
    }
}
