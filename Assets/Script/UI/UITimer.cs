﻿using UnityEngine;
using UnityEngine.UI;

public class UITimer : MonoBehaviour {

    private Text _time;

    private void Awake () {
        _time = GetComponent<Text>();
	}
	
    public void SetTextTime(string text)
    {
        _time.text = text;
    }

    public string GetTimeText()
    {
        return _time.text;
    } 
}
